# MAMPについての資料

## MAMPとは

授業では、PHPを利用する際に`MAMP`というものを利用します。

- `MAMP`とは？
  - ローカル環境(≒自分のパソコンんの中)にサーバーを構築することが可能です。

- `MAMP`の配布先
  - [こちら](https://www.mamp.info/en)

## MAMPのダウンロード

以下、MacOSの場合と、Windowsの場合の説明を記述します。
なお、下記情報は、2021年8月1日の状況なので、場合によっては、サイトのレイアウトやダウンロード方法が変わっているかもしれません。

### MacOSの場合

-  サイトからダウンロード
![001](/uploads/e2acb9470d534abb520a8c23a92f83b9/001.png)

![002](/uploads/a3b14f4a94dd1aa7be1ba94109e64cce/002.png)

- ダウンロードしたパッケージを実行↓
![003](/uploads/f0dbc1acadbf58fe071d95d7f7938fdc/003.png)

- 基本は続けるをクリック↓
![004](/uploads/c769f23d942595f5f76bfc92253e5b11/004.png)

![005](/uploads/7a8388f5671101b01bbc3d671f18e982/005.png)

![006](/uploads/96ccf26b2ca4f68f25ddfad3563ecae1/006.png)

- 何か聞かれたら、基本的にAgree↓
![007](/uploads/4f6ec6b0c54400698dba9725f5fb2d80/007.png)

- インストールが済んだら、アプリケーションの中に`MAMP`があることを確認する。↓
  - なお、`MAMP PRO`は不要なので、ゴミ箱に入れてしまってもok
![009](/uploads/c55c92e9195b8aa357057ba3f0454c03/009.png)

- `MAMP`フォルダの中にある、`MAMP.app`をクリックして実行↓
![010](/uploads/173442b5894a866da1ce3f7adffab0e2/010.png)

- 左上のpreferencesをクリック↓
![011](/uploads/ba0153ffa754e4ae82da288c46396c9e/011.png)

- Portsの`[80&3306]`をクリックして`ok`を押す
![012](/uploads/d65ea1771a08df162c36e7669bbad143/012.png)

- こんな感じ↓
![013](/uploads/710daba1a2233ab8daba25e13668f417/013.png)

- 右上の`Start`をクリック
![014](/uploads/fb809e10eab90ec180d8dda5a38bf0ff/014.png)

- 右上が緑になったらok(`MAMP`が起動している状態)
![015](/uploads/5dc8629fe20067e50fb14672294f4a85/015.png)

- MAMPが起動したら、（おそらく）自動的にブラウザが起動して、MAMPのページが出てくると思います。
- ※このページが見えている = 無事にMAMPが使えている状態です。
![016](/uploads/828abceda5466b71cf6a163259fd3994/016.png)

- 手動でMAMPのページを出すには、飛行機のマークをクリック。
![017](/uploads/03d38eb1bdf9ef915eba36a456e93471/017.png)


#### htdocsの確認

- `MAMP`フォルダの中の`htdocs`を確認
  - `MAMP`を起動した後に開くと、`index.php`が生成されます。**これは、削除してしまってください。**
![001](/uploads/52533793436d1b8bda31511001465a0c/001.png)




### Windowsの場合

